-- ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'mysqlaccount';
-- FLUSH PRIVILEGES;

create database if not exists test ;

use test;

create  table if not exists users (
id int(11) not null auto_increment,
name varchar(100) not null,
age int(3) not null,
email varchar(100) not null,
primary key (id)
);

insert into users(name, age, email) values ('abc', 22, 'abc@gmail.com');
insert into users(name, age, email) values ('def', 20, 'def@gmail.com');
insert into users(name, age, email) values ('xyz', 29, 'xyz@gmail.com');

-- drop database if exists test 